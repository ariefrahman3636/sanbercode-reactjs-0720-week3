import React, { Component } from 'react';

class HitungMundur extends Component {
    constructor (props) {
        super(props)
        this.state = {
            count: 10
        }
    }
    render() {
        const {count} = this.state
        return (
            <div style={{width: "40%", margin: "0 auto"}}>
                <p>Hitung mundur : {count}</p>
            </div>
        )
    }
    componentDidMount() {
        const {startCount} = this.props
        this.setState({
            count: startCount
        })
        this.myInterval = setInterval(()=>{
            this.setState({
                count: this.state.count - 1
            })
        }, 1000)
    }
    componentWillUnmount() {
        clearInterval(this.myInterval)
    }
}

export default HitungMundur